<?php
	/**
	 * Created by PhpStorm.
	 * User: dronz
	 * Date: 8.6.18
	 * Time: 17.41
	 */

	namespace dronz\datesbc;


	use yii\i18n\Formatter;

	class DateBcFormatter extends Formatter{

		public function asDateBc($value){
			if($result = DateBcHelper::explode($value)){
				$result['bc'] = $result['bc'] == '-' ? ' BC' : '';

				foreach(['day', 'month'] as $part){
					if(isset($result[$part]) && (!$result[$part] || $result[$part] == '00')){
						$result[$part] = FALSE;
					}
				}
				$result['month'] = $result['month'] ? date_create_from_format('m', $result['month'])->format('M') : '';

				return trim(strtr('day month yearbc', (array)$result));
			}
			else return $value;
		}
	}