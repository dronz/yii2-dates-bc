<?php
	/**
	 * Created by PhpStorm.
	 * User: dronz
	 * Date: 8.6.18
	 * Time: 17.56
	 */

	namespace dronz\datesbc;


	class DateBcHelper{

		public static function explode($value){
			if(preg_match("/(?P<bc>-?)(?P<year>\d{1,4})(?P<month>\d{2})(?P<day>\d{2})/", $value, $result)){
				return $result;
			}
			else return FALSE;
		}

		public static function implode(array $date){
			foreach(['day', 'month'] as $part){
				if(!isset($date[$part])){
					$date[$part] = '00';
				}
				elseif(strlen($date[$part]) == 1){
					$date[$part] = '0'.$date[$part];
				}
			}


			return isset($date['year']) ? $date['year'].$date['month'].$date['day'] : NULL;
		}
	}