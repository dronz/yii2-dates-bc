<?php
	/**
	 * Created by PhpStorm.
	 * User: dronz
	 * Date: 8.6.18
	 * Time: 16.59
	 */

	namespace dronz\datesbc;


	use yii\validators\Validator;

	class DateBcValidator extends Validator{

		protected function validateValue($value){
			if($result = DateBcHelper::explode($value)){
				if($result['month'] > 12){
					return ['month must be less then 12', []];
				}
				if($result['day'] > 31){
					return ['day must be less then 31', []];
				}
			}
			else{
				return ['not valid', []];
			}
		}
	}