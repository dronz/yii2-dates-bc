<?php
	/**
	 * Created by PhpStorm.
	 * User: dronz
	 * Date: 8.6.18
	 * Time: 15.01
	 */

	namespace dronz\datesbc;


	use yii\helpers\ArrayHelper;
	use yii\helpers\Html;
	use yii\widgets\MaskedInput;

	class DateBcWidget extends MaskedInput{

		public $mask = '99-99-[999]9';

		private $bc = FALSE, $date = NULL;

		public $options = [
			'class'       => 'form-control',
			'placeholder' => 'DD-MM-YYYY',
		];

		public function init(){
			parent::init();
			$value    = Html::getAttributeValue($this->model, $this->attribute);
			$this->bc = $value < 0;
			if($result = DateBcHelper::explode($value)){
				$this->date = trim($result['day'].'-'.$result['month'].'-'.$result['year'], '-');
			}

			$js = <<<JS
	$('input[name=date],input[name=bc]').change(function(){
		var block=$(this).closest('div.form-group');
		var bc=block.find('input[name=bc]');
		var date=block.find('input[name=date]');
		var hidden=block.find('input[type=hidden]');
		 var i=date.val().split('-');
		 var result=(
		 	(bc.prop('checked') ? '-' : '')
		 	+(i[2] ? i[2].replace(/_/g,''):'')
		 	+(i[1] ? i[1].replace(/_/g,'0'):'00')
		 	+(i[0] ? i[0].replace(/_/g,'0'):'00')
		 	);
		 if(result=='0000'){
		 	result='';
		 }
		 hidden.val(result);
		 console.log(result);
		 console.log(hidden,date);
	})
JS;
			$this->view->registerJs($js);

		}

		/**
		 * {@inheritdoc}
		 */
		public function run(){
			$this->registerClientScript();
			$options = $this->options;
			ArrayHelper::remove($this->options, 'id');
			ArrayHelper::remove($this->options, 'placeholder');
			ArrayHelper::remove($this->options, 'data-plugin-inputmask');
			echo Html::tag('div', Html::tag('span', Html::checkbox('bc', $this->bc).' BC', ['class' => 'input-group-addon']).
				Html::textInput('date', $this->date, $options), ['class' => 'input-group']);
			echo Html::activeInput('hidden', $this->model, $this->attribute, $this->options);
		}
	}