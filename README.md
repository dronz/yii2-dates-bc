Dates BC
========
Dates BC

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist dronz/yii2-dates-bc "*"
```

or add

```
"dronz/yii2-dates-bc": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
<?= \dronz\datesbc\AutoloadExample::widget(); ?>```